import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Container from '../Container/Container';
import { ReactComponent as Logo } from './images/logo.svg';
import { ReactComponent as Favorite } from './images/favorite.svg';
import { ReactComponent as Cart } from './images/cart.svg';
import { selectCart, selectFavorite } from '../../store/selectors';


const StyledHeader = styled.header`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(3,37,65, 1);
    min-height: 64px;
    width: 100%;
    transition: top 0.2s linear;
    position: fixed;
    top: 0;
`

const FlexSpaceWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 20px;
`
const CounterWrapper = styled.div`
    position: relative;
`
const Counter = styled.span`
    position: absolute;
    top: -2px;
    right: -10px;
    font-size: 16px;
    color: orange;
`
const NavList = styled.ul`
list-style: none;
display: flex;
justify-content: center;
align-items: center;
gap: 100px;
`

const NavItem = styled.span`
    color: #fff;
    text-transform: uppercase;
    font-size: 21px;

    :hover {
        text-decoration: underline;
    }
`

const StyledLink = styled(Link)`
    text-decoration: none;
`

const Header = () => {
    let cart = useSelector(selectCart);
    let favorite = useSelector(selectFavorite);
    return (
        <StyledHeader>
            <Container>
                <FlexSpaceWrapper>
                    <div className="header__logo">
                        <Link to="/">
                            <Logo/>
                        </Link>
                    </div>
                    <nav>
                        <NavList>
                            <li>
                                <StyledLink to='/'>
                                    <NavItem>Home</NavItem>
                                </StyledLink>
                            </li>
                            <li>
                                <StyledLink to='/favorite'>
                                    <NavItem>Favorite</NavItem>
                                </StyledLink>
                            </li>
                            <li>
                                <StyledLink to='/cart'>
                                    <NavItem>Cart</NavItem>
                                </StyledLink>
                            </li>
                        </NavList>
                    </nav>
                    <FlexSpaceWrapper>
                        <CounterWrapper className='favorite'>
                            <Link to='/favorite'>
                                <Favorite />
                                <Counter>{favorite.length}</Counter>
                            </Link>
                        </CounterWrapper>
                        
                        <CounterWrapper className='cart'>
                            <Link to='/cart'>
                                <Cart />
                                <Counter>{cart.length}</Counter>
                            </Link>
                        </CounterWrapper>
                        
                    </FlexSpaceWrapper>
                </FlexSpaceWrapper>
            </Container>
        </StyledHeader>
    )
}

export default Header