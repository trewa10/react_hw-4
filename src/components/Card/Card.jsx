import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Button from '../Button';
import {ReactComponent as FavoriteIcon} from './images/favorite.svg'
import { useDispatch, useSelector } from 'react-redux';
import { selectCart, selectGoodsCollection } from '../../store/selectors';
import { actionCart, actionFavorite, actionGoodsCollection, actionTryToCart, actionTryToRemoveCart, actionModalText, actionModalTitle, actionNeedRemoveItem, actionIsModalOpen } from '../../store/actions'

const CardElement = styled.div`
    box-sizing: border-box;
    width: 300px;
    height: 370px;
    border: 1px solid grey;
    border-radius: 10px;
    padding: 10px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`
const CardSection = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
`
const CardMain = styled(CardSection)`
    flex-direction: column;
    align-items: flex-start;
` 
const CardFooter = styled(CardSection)`
    /* position: absolute;
    bottom: 5px; */
` 
const StyledSpan = styled.span`
    font-size: ${props => props.fontSize};
` 
const ImgContainer = styled.div`
    margin: 5px 15px;
    align-self: flex-end;
    min-height: 200px;
    display: flex;
    align-items: center;
`

const ItemIMG = styled.img`
    width: 150px;
    height: auto;
`

const Card = ({name, price, imgUrl, article, color, isRemove, isInFavorite, textBtn, widthBtn}) => {
    const dispatch = useDispatch();
    const cart = useSelector(selectCart);
    const goodsCollection = useSelector(selectGoodsCollection);
    
    const addToFavorite = (article) => {
        let newGoodsCollection = goodsCollection.map(item => item.article === article ? {...item, isInFavorite: !item.isInFavorite} : item);
        let newCart = cart.map(item => item.article === article ? {...item, isInFavorite: !item.isInFavorite} : item);
        let newFavorite = newGoodsCollection.filter(item => item.isInFavorite);
        dispatch(actionFavorite(newFavorite))
        localStorage.setItem('favorite', JSON.stringify(newFavorite));
        dispatch(actionGoodsCollection(newGoodsCollection));
        dispatch(actionCart(newCart));
        localStorage.setItem('cart', JSON.stringify(newCart));
    }

    const addToCart = (article) => {
        let item = goodsCollection.filter(item => item.article === article)[0];
        dispatch(actionTryToCart(item));
        dispatch(actionModalText(item.name));
        dispatch(actionModalTitle('Do you want to add to cart?'));
        dispatch(actionNeedRemoveItem(false));
        dispatch(actionIsModalOpen(true));
    }

    const removeFromCart = (article) => {
        let item = cart.filter(item => item.article === article)[0];
        dispatch(actionTryToRemoveCart(item));
        dispatch(actionModalText(item.name));
        dispatch(actionModalTitle('Do you want to remove from cart?'));
        dispatch(actionNeedRemoveItem(true));
        dispatch(actionIsModalOpen(true));
      }


    return (
        <CardElement>
            <CardSection>
                <span>{name}</span>
                <Button width='auto' height='auto' onClick={() => addToFavorite(article)}>
                    <div>
                        <FavoriteIcon fill={isInFavorite ? 'orange' : 'white'} stroke={isInFavorite ? 'orange' : 'grey'}/>
                    </div>
                </Button>
            </CardSection>
            <CardMain>
                <StyledSpan fontSize='12px'>Article: {article}</StyledSpan>
                <ImgContainer>
                    <ItemIMG src={imgUrl} alt={name} />
                </ImgContainer>
                <StyledSpan>Color: {color}</StyledSpan>
            </CardMain>
            <CardFooter>
                <div>{price}$</div>
                <Button backgroundColor='grey' width={widthBtn} onClick={isRemove ? () => removeFromCart(article) : () => addToCart(article)}>{textBtn}</Button>
            </CardFooter>
        </CardElement>
    )
}



Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    imgUrl: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    isRemove: PropTypes.bool,
    isInFavorite: PropTypes.bool,
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string
}

export default Card