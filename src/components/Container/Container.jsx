import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledContainer = styled.div`
    width: 100%;
	padding-right: calc(16px * 0.5);
	padding-left: calc(16px * 0.5);
	margin-right: auto;
	margin-left: auto;

    @media (min-width: 576px) {
            max-width: 540px;
    }

    @media (min-width: 768px) {
        max-width: 720px;
    }

    @media (min-width: 992px) {
        max-width: 960px;
    }

    @media (min-width: 1200px) {
        max-width: 1140px;
    }

    @media (min-width: 1400px) {
        max-width: 1320px;
    }
`

const Container = ({children}) => {
    return <StyledContainer>{children}</StyledContainer>
}

Container.propTypes = {
    children: PropTypes.any
}

export default Container