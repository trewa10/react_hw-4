import styled from "styled-components";
import PropTypes from 'prop-types';
import {ReactComponent as LeftArrow} from "./images/left_arrow.svg";

const BackSpan = styled.span`
display: inline-block;
cursor: pointer;
margin-right: 10px;
transform: rotate(180deg);

:hover {
    transform: scale(1.2) rotate(180deg);
}

svg {
    width: 32px;
    height: 25px;
}
`

const BackBtn = ({goBack}) => {
    return (
        <BackSpan onClick={goBack}><LeftArrow/></BackSpan>
    )
}

BackBtn.propTypes = {
    goBack: PropTypes.func
}

export default BackBtn