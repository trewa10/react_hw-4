import PropTypes from 'prop-types';
import styled from 'styled-components';
import PageWrapper from "../../components/PageWrapper/PageWrapper";
import ItemsList from "../../components/ItemsList";
import BackBtn from '../../components/BackBtn';
import { selectCart } from '../../store/selectors';
import { useSelector } from 'react-redux';

const Price = styled.p`
    width: max-content;
    margin: 0px auto 20px;
    font-size: 1.3rem;
`

const CartPage = (props) => {
    const cart = useSelector(selectCart);

    const {textBtn, widthBtn, goBack} = props;
    const totalPrice = cart.reduce((acc, item) => acc + +item.price, 0)
    return (
        <PageWrapper>
            <h2><BackBtn goBack={goBack} />Your cart</h2>
            <Price>You have {cart.length} item(s) with total value {totalPrice}$</Price>
            <ItemsList itemsList={cart} isRemove={true} textBtn={textBtn} widthBtn={widthBtn}/>
        </PageWrapper>
    )
}

CartPage.propTypes = {
    textBtn: PropTypes.string.isRequired,
    widthBtn: PropTypes.string,
    goBack: PropTypes.func
}

export default CartPage;